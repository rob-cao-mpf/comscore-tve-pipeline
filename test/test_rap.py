import os
from raplib.api import Rap, RapError

# Create rap instance 
raplogin = {'api_url' : 'https://rap-dev.rentrak.com/api',
            'user_id': os.getenv('USER'),
            'org_id': 'groupm',
            'api_token': os.getenv('COMSCORE_API_TOKEN')}

r = Rap(**raplogin)
print(r.get_entities('network'))
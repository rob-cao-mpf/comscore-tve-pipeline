from setuptools import find_packages, setup



DESC = (
    "Simplify data extraction from Comscore TV-Essentials dataset"
)

URL = "https://gitlab.com/rob-cao-mpf/comscore-tve-pipeline"


setup(
    name="tvePypeline",
    version='0.1.0',
    python_requires=">=3.0",
    description=DESC,
    url=URL,
    author="Rob Cao",
    author_email="robin.cao@choreograph.com",
    maintainer="Rob Cao",
    maintainer_email="robin.cao@choreograph.com",
    license="MIT",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    install_requires=[
        "pyyaml",
        "raplib @ git+https://rap-dev.rentrak.com/git/raplib.git#egg=raplib"
    ],
)

import yaml
from pathlib import Path

## Create rap instance
import os
from raplib.api import Rap
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

rap = Rap(**{'api_url' : 'https://rap-dev.rentrak.com/api',
            'user_id': os.getenv('USER'),
            'org_id': 'groupm',
            'api_token': os.getenv('COMSCORE_API_TOKEN')})

class Pipeline:
    ''' Data pipeline with the Comscore API '''

    def __init__(self,config_path=None):
        if config_path:
            with open(config_path) as f:
                config = yaml.load(f, Loader=yaml.FullLoader) 
                
        self.folder = config['folder'] if config_path else None
        self.start = config['start'] if config_path  else None
        self.end = config['end'] if config_path  else None
        self.network = config['networks'] if config_path  else None
        self.market = config['markets'] if config_path  else None
        self.content = config['content'] if config_path  else None
        self.tag = config['targets'] if config_path  else None
        ##
        self.select_fields = config['select_fields'] if config_path  else None
        self.group_fields = config['group_fields'] if config_path  else None
        self.adschedule_id = config['adschedule_id'] if config_path  else None
        self.cdaypartset_id = config['cdaypartset_id'] if config_path  else None
        ##
        self._report_id = None
        

    def create_report(self):
        ## Necessary inputs
        if not self.tag:
            raise AttributeError('Please specify target list')
        if not self.network:
            raise AttributeError('Please specify network list')
        if not self.start:
            raise AttributeError('Please specify start date')
        if not self.end:
            raise AttributeError('Please specify end date')
        
        args = {
            'select_fields': self.select_fields,
            'group_fields': self.group_fields,
            'dataset_filter': self.dataset_filter(),
            'target_filter' : self.tag,
            'adschedule_id' : self.adschedule_id,
            'cdaypartset_id': self.cdaypartset_id
                }
        
        # Create report
        if self.adschedule_id:
            rap.wait_for_adschedule(self.adschedule_id)
        self._report_id = rap.create_report(**args)
        return self._report_id
    
    def download_report(self,report_id=None,path=None):
        ## If no report_id is provided, use class attribute
        report_id = report_id if report_id else self._report_id
        
        ## If no path is provided, use class attribute + rid
        path = '/'.join([self.folder,report_id]) + '.csv' if not path else path
        rap.wait_for_report(report_id)
        rap.download_report(report_id,'csv',path)
        return path
    
    def upload_report(self,report_id=None,url=None):
        ## If no report_id is provided, use class attribute
        report_id = report_id if report_id else self._report_id
        
        ## If no path is provided, use class attribute + rid
        url = '/'.join([self.folder,report_id]) + '.csv' if not url else url
        rap.wait_for_report(report_id)
        rap.create_report_upload(report_id,url,format='csv')
        return url
            
    
    def dataset_filter(self):
        '''Setting report dataset filter''' 
        
        ## Required filters
        network_filter = ' OR '.join([f'NETWORK_ID={net}' for net in set(self.network)])
        start_filter = f"NATIONAL_TIME>='{self.start} 00:00:00'"
        end_filter = f"NATIONAL_TIME<='{self.end} 23:59:59'"
        
        ## Optional filters
        content_type = {'national':1,'local':0}
        if content_type.get(self.content):
            content_filter = f"NATIONAL_CONTENT={content_type.get(self.content)}"
        else:
            content_filter = None

        if self.market:
            market_filter = " OR ".join([f"MARKET_ID={mkt}" for mkt in self.market])
        else:
            market_filter = None
            
        ## Join filters
        filters = [network_filter,start_filter,end_filter,market_filter,content_filter]
        dataset_filter = " AND ".join([f"({f})" for f in filters if f])
        return dataset_filter
            
        
def RunPipeline(config_path):
    ### 
    if config_path.endswith('.yml'):
        configs = [config_path]
    elif os.path.isdir(config_path):
        configs = [f'{config_path}/{cf}' for cf in os.listdir(config_path) 
                                         if cf.endswith('.yml')]

    ### Create report for each config file
    pplns = [Pipeline(config) for config in configs]
    report_ids = [ppln.create_report() for ppln in pplns]

    ### Fetch reports (local or s3)
    paths = []
    for i,ppln in enumerate(pplns):
        if ppln.folder.startswith('s3'):     ## s3 upload
            paths.append(ppln.upload_report())
        else:                                ## normal download
            Path(ppln.folder).mkdir(parents=True, exist_ok=True)
            paths.append(f'{os.getcwd()}/{ppln.download_report()}')
        ##
        print(f'Reports completed: {i+1}/{len(report_ids)}', 
              end="\r", flush=True)
    print('\n')
    
    return paths
        
        